const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const bodyparser = require("body-parser");
const mime = require('mime');
const path = require('path');
const mongoose = require('mongoose')

const app = express();
app.get('/assets/js/index.js', (req, res) => {
    res.setHeader('Content-Type', mime.getType('index.js'));
    res.sendFile(__dirname + '/assets/js/index.js');
});

dotenv.config( { path : 'config.env'} )
const PORT = process.env.PORT || 8080

// log requests
app.use(morgan('tiny'));

// mongodb connection

const db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection error'));
console.log(process.env.MONGO_URL)
mongoose.connect(`${process.env.MONGO_URI}`).catch((e) => console.log(e))
db.once('open', () => console.log('Database connected'));

// parse request to body-parser
app.use(bodyparser.urlencoded({ extended : true}))

// set view engine
app.set("view engine", "ejs")
//app.set("views", path.resolve(__dirname, "views/ejs"))

// load assets
app.use('/css', express.static(path.resolve(__dirname, "assets/css")))
app.use('/img', express.static(path.resolve(__dirname, "assets/img")))
app.use('/js', express.static(path.resolve(__dirname, "assets/js")))

// load routers
app.use('/', require('./server/routes/router'))

app.listen(PORT, ()=> { console.log(`Server is running on http://localhost:${PORT}`)});